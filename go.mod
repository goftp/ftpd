module goftp.io/ftpd

go 1.20

require (
	gitea.com/goftp/leveldb-perm v0.0.0-20190711092750-00b79e6da99c
	gitea.com/lunny/tango v0.6.1
	gitea.com/tango/binding v0.0.0-20200204091933-f90d5bac28d2
	gitea.com/tango/flash v0.0.0-20190606021323-2b17fd0aed7c
	gitea.com/tango/renders v0.0.0-20191027160057-78fc56203eb4
	gitea.com/tango/session v0.0.0-20190606020146-89f560e05167
	gitea.com/tango/xsrf v0.0.0-20190606015726-fb1b2fb84238
	github.com/lunny/log v0.0.0-20160921050905-7887c61bf0de
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/syndtr/goleveldb v1.0.0
	github.com/unknwon/goconfig v0.0.0-20190425194916-3dba17dd7b9e
	goftp.io/server/v2 v2.0.0
)

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e // indirect
	github.com/go-xweb/uuid v0.0.0-20140604020037-d7dce341f851 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/minio/minio-go/v6 v6.0.46 // indirect
	github.com/minio/sha256-simd v0.1.1 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c // indirect
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/unknwon/com v1.0.1 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190726091711-fc99dfbffb4e // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/ini.v1 v1.42.0 // indirect
)
